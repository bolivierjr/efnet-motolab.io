Title: what is #motorcycles?
Date: 2017-07-16 16:24
save_as: index.html
status: hidden

We are an EFnet based conversation channel devoted to the discussion and pursuit of motorcycling. 
Owning a bike is not a prerequisite to join, but can be a side effect.
